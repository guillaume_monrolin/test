﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP4
{
    abstract class Pokemon
    {
        public string nom { get; set; }
        public double poids { get; set; }
        public int nbMembres { get; set; }

        protected Pokemon(string nom, double poids, int nbMembres)
        {
            this.nom = nom;
            this.poids = poids;
            this.nbMembres = nbMembres;
        }

        public abstract double vitesse();
        public override string ToString()
        {
            return "\n\t- Nom : " + this.nom + "\n\t- Poids : " + this.poids + "\n\t- Vitesse : " + vitesse();
        }
    }
}
