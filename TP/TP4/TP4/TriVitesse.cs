﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP4
{
    class TriVitesse : IComparer
    {
        public int Compare(object x, object y)
        {
            Pokemon p1 = x as Pokemon;
            Pokemon p2 = y as Pokemon;
            if (p1.vitesse() > p2.vitesse())
                return 1;
            else if (p1.vitesse() < p2.vitesse())
                return -1;
            return 0;
        }
    }
}
