﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP4
{
    class PokemonCasanier : Pokemon
    {
        public double taille { get; set; }
        public double heureTele { get; set; }

        public PokemonCasanier(string nom, double poids, double taille, int nbMembres, double heureTele)
            : base(nom, poids, nbMembres)
        {
            this.taille = taille;
            this.heureTele = heureTele;
        }

        public override double vitesse()
        {
            return this.nbMembres * this.taille * 3;
        }

        public override string ToString()
        {
            return "Pokemon Casanier : " + base.ToString() + "\n\t- Nombre de pattes : " + this.nbMembres
                + "\n\t- Taille : " + this.taille
                + "\n\t- Nombres d'heure passé devant la télé par jour : " + this.heureTele;
        }
    }
}
