﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP4
{
    class TabPokemon : ArrayList
    {
        public Pokemon plusRapide()
        {
            Pokemon tmp = this[0] as Pokemon;
            foreach (Pokemon p in this)
            {
                if (p.vitesse() > tmp.vitesse())
                {
                    tmp = p;
                }
            }
            return tmp;
        }

        public override void Sort()
        {
            base.Sort(new TriVitesse());
        }

        public override void Sort(IComparer comparer)
        {
            base.Sort(comparer);
        }

        public void add(Pokemon p)
        {
            this.Add(p);
        }

        public override string ToString()
        {
            string toString = "-----------------------------------\nTab Pokemon : \n";
            foreach (Pokemon p in this)
            {
                toString += p.ToString() + "\n\n";
            }

            return toString;
        }
    }
}
