﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP4
{
    class Program
    {
        static void Main(string[] args)
        {
            TabPokemon tab = new TabPokemon();
            PokemonSportif s = new PokemonSportif("Pikachu", 18, 0.85f, 2, 120);
            PokemonCasanier c = new PokemonCasanier("Salameche", 12, 0.65f, 2, 8);
            PokemonMer m = new PokemonMer("Rondoudou", 45, 2);
            PokemonCroisiere cr = new PokemonCroisiere("Bulbizare", 15, 3);
            
            tab.Add(s);
            tab.Add(c);
            tab.Add(m);
            tab.Add(cr);
            Console.WriteLine(tab);
            
            Console.WriteLine("POKEMON LE PLUS RAPIDE");
            Console.WriteLine(tab.plusRapide());

            Console.WriteLine("TRI vitesse\n\n ");
            tab.Sort(); foreach (Pokemon p in tab) Console.WriteLine(p);

            Console.WriteLine("TRI SUR POIDS\n\n ");
            tab.Sort(new TriPoids());
            
            foreach (Pokemon p in tab)
                Console.WriteLine(p);
            
        }
    }
}
