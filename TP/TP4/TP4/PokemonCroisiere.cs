﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP4
{
    class PokemonCroisiere : Pokemon
    {
        public PokemonCroisiere(string nom, double poids, int nbMembres)
            : base(nom, poids, nbMembres)
        {

        }

        public override double vitesse()
        {
            return (this.poids / 25 * this.nbMembres) / 2;
        }

        public override string ToString()
        {
            return "Pokemon Croisière : " + base.ToString() + "\n\t- Nombre de nageoire : " + this.nbMembres;
        }
    }
}
