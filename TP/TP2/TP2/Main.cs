﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("How many elements ?");
            TabClass tab = new TabClass(int.Parse(Console.ReadLine()));
            tab.remplirTableau();
            tab.sort();
            tab.print();
        }
    }
}
