﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2
{
    class TabClass
    {
        private int maxSize;
        private int size;
        private int[] tab;

        public int MaxSize
        {
            get { return maxSize; }
            set { maxSize = value; }
        }

        public int Size
        {
            get { return size; }
        }

        public int this[int index]
        {
            get { return tab[index]; }
            set { tab[index] = value; }
        }

        public TabClass (int maxSize)
        {
            this.maxSize = maxSize;
            this.size = 0;
            this.tab = new int[maxSize];
        }

        public void remplirTableau ()
        {
            bool shouldContinue = true;
            string value;
            while (shouldContinue && this.maxSize >= size)
            {
                Console.WriteLine("Do you want to continue ? (o/n)");
                value = Console.ReadLine();
                if (value == "o")
                {
                    Console.Write("Value : ");
                    this[size] = int.Parse(Console.ReadLine());
                    size++;
                }
                else
                    shouldContinue = false;
            }
        }

        public void print()
        {
            Console.Write("[");
            for (int i = 0; i < size; i++)
            {
                Console.Write("{0}", this[i]);
                if (i < size - 1 )
                    Console.Write(", ");
            }
            Console.Write("]");
        }

        public void sort()
        {
            int tmp, index;
            for (int i = 0; i < size; i++)
            {
                tmp = this[i];
                index = i;
                for (int j = i; j < size; j++)
                {
                    if( tmp > this[j])
                    {
                        tmp = this[j];
                        index = j;
                    }
                }
                this[index] = this[i];
                this[i] = tmp;
            }
        }

        public void lazySort()
        {

            Array.Sort(tab);
        }
    }
}
