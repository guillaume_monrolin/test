﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercice2
{
    class Article
    {
        private int prix = 0;

        public int Prix
        {
            get { return prix; }
            set { prix = value; }
        }

        public void affichePrix()
        {
            Console.WriteLine("Prix: {0} euros", Prix);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Article A = new Article();
            A.Prix = 10;
            A.affichePrix();
        }
    }
}
