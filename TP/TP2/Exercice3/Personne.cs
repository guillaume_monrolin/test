﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercice3
{
    class Personne
    {
        string [] m_Noms;   // Tableau privé interne qui contient les noms des personnes. 
        int m_NbElt;    // nombre d’éléments dans le tableau   
        int m_Max;   // nombre maximum d’éléments 

        // Le constructeur qui initialise le tableau.   
        public Personne(int Max)
        {
            m_Noms = new string[Max];
            m_Max = Max;
            m_NbElt = 0;
        }

        // L'indexeur qui retourne l'index à partir du nom.   
        public int this[string Nom]
        {
            get
            {
                for (int i = 0; i < m_NbElt; i++)
                {
                    if (m_Noms[i] == Nom) return i;
                }
                return -1;
            }
        }

        // L'indexeur qui retourne ou affecte le nom à partir de l’index.   
        public string this[int i]
        {
            get
            {
                if ( i < m_NbElt) return m_Noms[i];
                return "not found";
            }
            set { m_Noms[i] = value; m_NbElt++; }
        } 
}
}
