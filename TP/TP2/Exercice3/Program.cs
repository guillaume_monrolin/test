﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercice3
{
    class Program
    {
        static void Main(string[] args)
        {
            Personne Tableau = new Personne(4);
            Tableau[0] = "Anna";
            Tableau[1] = "Ingrid";
            Tableau[2] = "Maria";
            Tableau[3] = "Ulrika";
            Console.WriteLine(Tableau[1]);   // Affiche "Ingrid"    
            Console.WriteLine(Tableau["Maria"]);  //Affiche 2    
            Console.WriteLine(Tableau[10]);   // Affiche null    
            Console.WriteLine(Tableau["Toto"]);  //Affiche -1 
        }
    }
}
