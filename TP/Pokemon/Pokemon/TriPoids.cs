﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pokemon
{
    class TriPoids : IComparer
    {
        public TriPoids() { }
        public int Compare(Object x, Object y)
        {
            Pokemon p1 = x as Pokemon;
            Pokemon p2 = y as Pokemon;
            if (p1.poids > p2.poids)
                return 1;
            else if (p1.poids == p2.poids)
                return 0;
            else
                return -1;
        }
    }
}
