﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pokemon
{
    class PokemonSportif : Pokemon
    {
        public double taille { get; set; }
        public double frequenceCardiaque { get; set; }

        public PokemonSportif(string nom, double poids, double taille, int nbMembres, double frequence)
            : base(nom, poids, nbMembres)
        {
            this.taille = taille;
            this.frequenceCardiaque = frequence;
        }

        public override double vitesse()
        {
            return this.nbMembres * this.taille * 3;
        }

        public override string ToString()
        {
            return "Pokemon Sportif" + base.ToString() + "\n\t- Nombre de pattes : " + this.nbMembres 
                + "\n\t- Taille : " + this.taille 
                + "\n\t- Frequence cardiaque : " + this.frequenceCardiaque;
        }
    }
}
