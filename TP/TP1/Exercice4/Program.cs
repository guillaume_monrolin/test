﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercice4
{
    class Program
    {
        static int[,] newMatInt(int width, int height)
        {
            int[,] mat = new int[height, width];
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    Console.Write("[{0}, {1}] : ", i, j);
                    mat[i,j] = int.Parse(Console.ReadLine());
                }
            }
            return mat;
        }

        static void printMat(int[,] mat)
        {
            Console.Write("[");
            for (int i = 0; i < mat.GetLength(0); i++)
            {
                Console.Write("[");
                for (int j = 0; j < mat.GetLength(1); j++)
                {
                    if (j < mat.GetLength(1) - 1)
                        Console.Write("{0}, ", mat[i,j]);
                    else
                        Console.Write("{0}", mat[i,j]);
                }
                Console.Write("]");
            }
            Console.Write("]");
        }
        static void Main(string[] args)
        {
            int[,] mat =  newMatInt(int.Parse(Console.ReadLine()), int.Parse(Console.ReadLine()));
            printMat(mat);
        }
    }
}
