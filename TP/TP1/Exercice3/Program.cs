﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercice3
{
    class Program
    {
        static int[] newIntArray(int size)
        {
            int[] array = new int[size];
            for (int i = 0; i < size; i++)
            {
                Console.Write("[{0}] : ", i);
                array[i] = int.Parse(Console.ReadLine());
                Console.WriteLine();
            }
            return array;
        }

        static void printArray(int[] array)
        {
            Console.Write("[");
            for (int i = 0; i < array.Length; i++)
            {
                if (i < array.Length - 1)
                    Console.Write("{0}, ", array[i]);
                else
                    Console.Write("{0}", array[i]);
            }
            Console.Write("]");
        }

        static void sort(ref int[] array)
        {
            for (int i = 0; i < array.Length - 1; i++)
                for (int j = i + 1; j < array.Length; j++)
                    if (array[i] > array[j])
                    {
                        int tmp = array[i];
                        array[i] = array[j];
                        array[j] = tmp;
                    }
        }
        static void Main(string[] args)
        {
            Console.Write("How many element(s) : ");
            int size = int.Parse(Console.ReadLine());

            int[] array = newIntArray(size);
            sort(ref array);
            printArray(array);
        }
    }
}
