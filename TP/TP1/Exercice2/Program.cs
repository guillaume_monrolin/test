﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercice2
{
    class Program
    {
        static void readString(out string s)
        {
            s = Console.ReadLine();
        }
        static void Main(string[] args)
        {
            string s = "";
            readString(out s);

            Console.WriteLine("Your string is : {0}", s);
        }
    }
}
