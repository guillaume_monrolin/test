﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercice5
{
    class Program
    {
        static int[][] newJaggediMat()
        {
            Console.Write("How many raws? ");
            int width = int.Parse(Console.ReadLine());
            int height;
            int[][] mat = new int[width][];
            for (int i = 0; i < width; i++)
            {
                Console.Write("How many columns for the raw {0}:", i);
                height = int.Parse(Console.ReadLine());
                mat[i] = new int[height];
                for (int j = 0; j < height; j++)
                {
                    Console.Write("[{0}][{1}] : ", i, j);
                    mat[i][j] = int.Parse(Console.ReadLine());
                }
            }
            return mat;
        }
        static void printArray(int[] array)
        {
            Console.Write("[");
            for (int i = 0; i < array.Length; i++)
            {
                if (i < array.Length - 1)
                    Console.Write("{0}, ", array[i]);
                else
                    Console.Write("{0}", array[i]);
            }
            Console.Write("]");
        }

        static void printMat(int[][] mat)
        {
            for(int i = 0; i < mat.Length; i++)
            {
                Console.Write("[");
                printArray(mat[i]);
                Console.Write("]");
            }
        }
        static void Main(string[] args)
        {
            int[][] mat = newJaggediMat();
            printMat(mat);
        }
    }
}
