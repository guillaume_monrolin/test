﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP1
{
    class Program
    {
        static void switchNumber(ref int a, ref int b)
        {
            int tmp = a;
            a = b;
            b = tmp;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Give me the value of a : ");
            int a = int.Parse(Console.ReadLine());

            Console.WriteLine("\nGive me the value of b : ");
            int b = int.Parse(Console.ReadLine());

            switchNumber(ref a, ref b);
            Console.WriteLine("\na : {0} b : {1}", a, b);
            
        }
    }
}
