﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercice3
{
    class Controleur
    {
        public string name;
        public string lieu;
        public int nbMachine;

        public Controleur(string name, string lieu)
        {
            this.name = name;
            this.lieu = lieu;
            nbMachine = 0;
        }

        public void onMachineSurchauffeEvent(object sender, SurchauffeEvent e)
        {
            nbMachine++;
            Console.Beep();
            Console.WriteLine(sender + " est en surchauffe");
        }

        public override string ToString()
        {
            return "Controleur " + name + " lieu : " + lieu + " nombre de machine en surchauffe " + nbMachine;
        }
    }
}
