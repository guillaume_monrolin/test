﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercice3
{
    class Program
    {
        static void Main(string[] args)
        {
            Machine m1 = new Machine("Machine 1", 10, 15);
            Machine m2 = new Machine("Machine 2", 10, 15);
            Machine m3 = new Machine("Machine 3", 10, 15);

            Controleur c1 = new Controleur("Controleur 1", "atelier");

            m1.controleur += c1.onMachineSurchauffeEvent;
            m2.controleur += c1.onMachineSurchauffeEvent;
            m3.controleur += c1.onMachineSurchauffeEvent;

            //m1.temperature = 16;
            //c1.ToString();
        }
    }
}
