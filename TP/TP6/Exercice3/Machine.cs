﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercice3
{
    class Machine
    {
        public string name;
        public int temperatureMax;
        public int temperature
        {
            get { return temperature; }
            set
            {
                temperature = value;
                if (temperature >= temperatureMax)
                    toHot();
            }
        }

        public event onMachineIsToHotEventHandler controleur;
        public delegate void onMachineIsToHotEventHandler(object sender, SurchauffeEvent e);

        public Machine(string name, int temperatureMax, int temperature)
        {
            this.name = name;
            this.temperature = temperature;
            this.temperatureMax = temperatureMax;
        }

        private void toHot()
        {
            controleur(this, new SurchauffeEvent());
        }

        public override string ToString()
        {
            return "Machine " + name + "est à la température " + temperature;
        }
    }
}
