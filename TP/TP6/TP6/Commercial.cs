﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP6
{
    class Commercial : Collaborateur
    {
        public int baseSalaire { get; set; }
        public int chiffreAffaire { get; set; }
        static public int t2 { get; set; }

        static Commercial()
        {
            t2 = int.Parse(Console.ReadLine());
        }

        public Commercial(String name, String dateNaiss, int baseSalaire, int chiffreAffaire): base(name, dateNaiss)
        {
            this.baseSalaire = baseSalaire;
            this.chiffreAffaire = chiffreAffaire;

            this.salaire = baseSalaire + chiffreAffaire * (t2 / 100);
        }
    }
}
