﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP6
{
    class TriAge : IComparer
    {
        public int Compare(Object x, Object y)
        {
            return (x as Collaborateur).dateNaiss > (y as Collaborateur).dateNaiss ? 1 : 
                (x as Collaborateur).dateNaiss < (y as Collaborateur).dateNaiss ? -1 : 0;
        }
    }
}
