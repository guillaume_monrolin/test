﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP6
{
    class Employe : Collaborateur
    {
        static private int t1;
        public int nbHeure { get; set; }
        public int tarifHorraire { get; set; }

        static Employe()
        {
            t1 = int.Parse(Console.ReadLine());
        }

        public Employe(String name, String dateNaiss, int tarif, int nbHeure) : base(name, dateNaiss)
        {
            this.tarifHorraire = tarif;
            this.nbHeure = nbHeure;
            this.salaire = nbHeure <= 35 ? nbHeure * tarifHorraire : 35 * tarifHorraire + (nbHeure - 35) * (1 + (t1 / 100));
        }


    }
}
