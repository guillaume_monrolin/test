﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP6
{
    class TabCollaborateur: ArrayList
    {
        public TabCollaborateur(): base() { }

        public Collaborateur PlusHautSal(string type = "Collaborateur")
        {
            Type selectedType = Type.GetType("TP6." + type);
            Collaborateur max = chargerMax(type);
            if(max != null)
            {
                foreach (Collaborateur c in this)
                {
                    if (c.GetType().Equals(selectedType) && c.salaire > max.salaire)
                        max = c;
                }
            }
            return max;
        }

        private Collaborateur chargerMax(string type)
        {
            foreach (Collaborateur c in this)
            {
                if (c.GetType().Equals(Type.GetType("TP6." + type)))
                    return c;
            }
            return null;
        }

        public override void Sort()
        {
            base.Sort(new TriAge());
        }
    }
}
