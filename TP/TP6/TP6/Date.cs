﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP6
{
    class Date
    {
        public int date { get; set; }

        public Date(String s)
        {
            date = int.Parse(s);
        }

        public static Boolean operator >(Date d1, Date d2)
        {
            string s1 = "" + d1.date;
            string s2 = "" + d2.date;
            int i1 = int.Parse(s1.Substring(4) + s1.Substring(2, 2) + s1.Substring(0, 2));
            int i2 = int.Parse(s2.Substring(4) + s2.Substring(2, 2) + s2.Substring(0, 2));
            return i1 > i2;
        }

        public static Boolean operator <(Date d1, Date d2)
        {
            return !(d1 > d2);
        }

        public override string ToString()
        {
            return date.ToString();
        }
    }
}
