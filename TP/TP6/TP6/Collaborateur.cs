﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP6
{
    class Collaborateur
    {
        public String nom{ get; }
        public Date dateNaiss { get; set; }

        public int salaire { get; set; }

        public Collaborateur(String nom, String dateNaiss)
        {
            this.nom = nom;
            this.dateNaiss = new Date(dateNaiss);
        }

        public override string ToString()
        {
            return nom + ", " + dateNaiss + ", " + salaire;
        }
    }
}
