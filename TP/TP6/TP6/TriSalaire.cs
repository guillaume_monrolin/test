﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP6
{
    class TriSalaire : IComparer
    {
        public int Compare(Object x, Object y)
        {
            return (x as Collaborateur).salaire > (y as Collaborateur).salaire ? 1 : 
                (x as Collaborateur).salaire < (y as Collaborateur).salaire ? -1 : 0;
        }
    }
}
