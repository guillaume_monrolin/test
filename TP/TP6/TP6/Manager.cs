﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP6
{
    class Manager : Collaborateur
    {
        public int baseSalaire { get; set; }
        public int prime { get; set; }

        public Manager(String name, String dateNaiss, int salaire, int prime) : base(name, dateNaiss)
        {
            this.baseSalaire = salaire;
            this.prime = prime;
            this.salaire = baseSalaire + prime;
        }
    }
}
