﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasesMilitaires
{
    class Program
    {
        static void Main(string[] args)
        {
            BaseMilitataire b1 = new BaseMilitataire("Wallice");
            Avion a1 = new Avion("Mouche");
            Soldat s1 = new Soldat("Charlie");
            b1.defendeur += a1.onAttaqueEventRecieve;
            b1.defendeur += s1.onAttaqueEventRecieve;
            a1.detruite += b1.onDestroyEventRecieve;
            s1.detruite += b1.onDestroyEventRecieve;
            b1.estAttaque();
            s1.estDetruit();
        }
    }
}
