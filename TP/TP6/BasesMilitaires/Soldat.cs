﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasesMilitaires
{
    class Soldat: Compagnie
    {
        public override event onDestroyEventHandler detruite;
        public Soldat(String name): base(name) { }

        public override void estDetruit()
        {
            if (detruite != null)
                detruite(this, new OnDestroyEvent());
        }

        public override void onAttaqueEventRecieve(object sender, OnAttaqueEvent e)
        {
            Console.WriteLine(this + " cours à la rescousse de la " + sender);
        }

        public override string ToString()
        {
            return "Compagnie de soldats " + this.name;
        }

        
    }
}
