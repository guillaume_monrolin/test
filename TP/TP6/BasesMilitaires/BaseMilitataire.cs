﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasesMilitaires
{
    class BaseMilitataire
    {
        public event onAttaqueEventHandler defendeur;
        public delegate void onAttaqueEventHandler(object sender, OnAttaqueEvent e);

        private string name;

        public BaseMilitataire(String name)
        {
            this.name = name;
        }

        public void estAttaque()
        {
            Console.WriteLine("La base militaire de " + this.name);
            if ( defendeur != null)
                defendeur(this, new OnAttaqueEvent());
        }

        public void onDestroyEventRecieve(object sender, OnDestroyEvent e)
        {
            Console.WriteLine(sender + " a été détruite en défendant la " + this);
        }

        public override string ToString()
        {
            return "base militaire " + this.name;
        }
    }
}
