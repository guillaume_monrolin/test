﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasesMilitaires
{
    abstract class Compagnie
    {
        protected string name;
        abstract public event onDestroyEventHandler detruite;
        public delegate void onDestroyEventHandler(object sender, OnDestroyEvent e);

        public Compagnie(String name)
        {
            this.name = name;
        }

        public abstract void onAttaqueEventRecieve(object sender, OnAttaqueEvent e);

        public abstract void estDetruit();
    }
}
