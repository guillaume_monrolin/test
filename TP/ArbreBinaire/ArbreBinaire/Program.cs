﻿using ArbreBinaire;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArbreBinaire
{
    class Program
    {
        static void Main(string[] args)
        {

            BinaryTree<int> abinInt = new BinaryTree<int>(0);

            abinInt.addElement(1);
            abinInt.addElement(2);
            abinInt.addElement(3);
            abinInt.addElement(4);
            abinInt.addElement(5);
            abinInt.addElement(6);
            abinInt.addElement(7);
            abinInt.addElement(8);
            abinInt.addElement(9);

            BinaryTree<int>.displayBinaryTree(abinInt);
        }
    }
}
