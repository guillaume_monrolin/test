﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArbreBinaire
{
    class BinaryTree<T> where T : IComparable
    {
        private Boolean isEmpty
        {
            get
            {
                return node == null;
            }
        }

        private Boolean isLeaf
        {
            get
            {
                return leftNode == null && rightNode == null;
            }
        }

        private T node { get; set; }

        public BinaryTree<T> leftNode { get; set; }

        public BinaryTree<T> rightNode { get; set; }

        public BinaryTree(T element)
        {
            node = element;
            rightNode = null;
            leftNode = null;
        }

        public void addElement(T element)
        {
            if(this.isEmpty)
            {
                node = element;
            }
            else
            {
                if (node.CompareTo(element) <= 0)
                {
                    if (leftNode == null)
                        leftNode = new BinaryTree<T>(element);
                    else
                        leftNode.addElement(element);
                }
                else
                {
                    if (rightNode == null)
                        leftNode = new BinaryTree<T>(element);
                    else
                        rightNode.addElement(element);
                }
            }
        }

        public override string ToString()
        {
            return node.ToString();
        }

        public static void displayBinaryTree(BinaryTree<T> currentTree)
        {
            if (!currentTree.isLeaf)
            {
                Console.Write(currentTree.node.ToString());
                if(currentTree.leftNode != null)
                    displayBinaryTree(currentTree.leftNode);
                if(currentTree.rightNode != null)
                    displayBinaryTree(currentTree.rightNode);
            }
        }

    }
}
