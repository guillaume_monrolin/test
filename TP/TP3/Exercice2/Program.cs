﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercice2
{
    class Program
    {
        static void Main(string[] args)
        {
            Fraction a = new Fraction();
            Console.WriteLine(a);
            Fraction b = new Fraction(2, -6);
            Console.WriteLine(b);
            Fraction c = new Fraction(8, 3);
            a = b + c;
            Console.WriteLine(a);
            a = b + 3;
            Console.WriteLine(a);
            a = 7 + b;
            Console.WriteLine(a);
        }
    } 
    
    class Fraction
    {
        private int num { get; set; }
        private int den { get; set; }

        public Fraction()
        {
            num = 0;
            den = 1;
        }

        public Fraction(int num, int den)
        {
            if (den < 0)
            {
                this.num = - num / pgcd(num, den);
                this.den = - den / pgcd(num, den);
            }
            else
            {
                this.num = num / pgcd(num, den);
                this.den = den / pgcd(num, den);
            }
        }

        public static Fraction operator+ (Fraction a, Fraction b)
        {
            if (a.den != b.den)
            {
                return new Fraction((a.num * b.den) + (b.num * a.den), a.den * b.den);
            }
            return new Fraction(a.num + b.num, a.den);
        }

        public static Fraction operator +(Fraction a, int nb)
        {
            return new Fraction(a.num + (nb * a.den), a.den);
        }

        public static Fraction operator +(int nb, Fraction a)
        {
            return new Fraction(a.num + (nb * a.den), a.den);
        }

        override public string ToString()
        {
            return this.num + "/" + this.den;
        }

        private static int pgcd(int a, int b)
        {
            int temp = a % b;
            if (temp == 0)
                return b;
            return pgcd(b, temp);
        }

    }
}
