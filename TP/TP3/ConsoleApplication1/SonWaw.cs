﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class SonWaw : Son
    {
        private int bits { get; set; }

        public SonWaw(int bits, string artiste, string album, string titre, int numero, int duree, int frequence)
            : base(artiste, album, titre, numero, duree, frequence)
        {
            this.bits = bits;
        }

        public override string toString()
        {
            return base.toString() + 
                "\nBits : " + bits;
        }
    }
}
