﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Son
    {
        protected string artiste { get; set; }
        protected string album { get; set; }
        protected string titre { get; set; }
        protected int numero { get; set; }
        protected int duree { get; set; }
        protected int frequence { get; set; }
        
        public Son(string artiste, string album, string titre, int numero, int duree, int frequence)
        {
            this.artiste = artiste;
            this.album = album;
            this.titre = titre;
            this.numero = numero;
            this.duree = duree;
            this.frequence = frequence;
        }

        public virtual string toString()
        {
            return "Artiste : " + artiste +
                "\nAlbum : " + album +
                "\nTitre : " + titre +
                "\nNumero : " + numero +
                "\nDuree : " + duree +
                "\nFrequence : " + frequence;
        }
    }
}
