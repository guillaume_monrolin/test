﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Playlist
    {
        private Son[] sons;
        private int nbSons;
        private int nbMaxSons;

        public Son this[int i]
        {
            get { return sons[i]; }
            set { sons[i] = value; }
        }

        public Playlist(int max)
        {
            nbMaxSons = max;
            nbSons = 0;
            sons = new Son[max];
        }

        public void add(Son son)
        {
            this[nbSons] = son;
        }

        public void remplirPlaylist()
        {
            bool shouldContinue = true;
            string value;
            // properties
            string album, artiste, titre;
            int numero, duree, frequence, valSupp;

            while (shouldContinue && this.nbMaxSons >= nbSons)
            {
                Console.Write("Do you want to continue ? (o/n) ");
                value = Console.ReadLine();
                if (value == "o")
                {
                    Console.Write("waw or mp4 ? ");
                    value = Console.ReadLine();

                    Console.Write("Artiste : ");
                    artiste = Console.ReadLine();
                    Console.Write("\nAlbum : ");
                    album = Console.ReadLine();
                    Console.Write("\nTitre : ");
                    titre = Console.ReadLine();
                    Console.Write("\nNuméro : ");
                    numero = int.Parse(Console.ReadLine());
                    Console.Write("\nDurée : ");
                    duree = int.Parse(Console.ReadLine());
                    Console.Write("\nFréquence : ");
                    frequence = int.Parse(Console.ReadLine());
                    if (value == "waw")
                    {
                        Console.Write("\nBits : ");
                        valSupp = int.Parse(Console.ReadLine());
                    }
                    else
                    {
                        Console.Write("\nDébit : ");
                        valSupp = int.Parse(Console.ReadLine());
                    }
                    this[nbSons] = new SonWaw(valSupp, album, artiste, titre, numero, duree, frequence);
                    nbSons++;
                }
                else
                    shouldContinue = false;
            }
        }

        public string toString()
        {
            string ret = "";
            for (int i = 0; i < nbSons; i++)
            {
                ret = ret + this[i].toString();
            }
            return ret;
        }

    }
}
