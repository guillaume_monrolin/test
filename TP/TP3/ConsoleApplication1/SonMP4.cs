﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class SonMP4: Son
    {
        private const string VBR = "VBR";
        private const string STEREO = "stereo";

        private int debit { get; set; }

        public SonMP4(int debit, string artiste, string album, string titre, int numero, int duree, int frequence)
            : base(artiste, album, titre, numero, duree, frequence)
        {
            this.debit = debit;
        }

        public override string toString()
        {
            return base.toString() +
                "\nDebit : " + debit;
        }
    }
}
